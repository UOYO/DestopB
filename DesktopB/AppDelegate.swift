//
//  AppDelegate.swift
//  DesktopB
//
//  Created by 李伟 on 2017/7/20.
//  Copyright © 2017年 liwei. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {
    
    let status = NSStatusBar.system.statusItem(withLength: NSStatusItem.variableLength)
    var item: NSStatusItem!
    let menu = NSMenu()
    let childmenu = NSMenu()
    var timer: Timer?
    
    func applicationDidFinishLaunching(_ aNotification: Notification) {
        
        if let button = status.button {
            button.image = NSImage(imageLiteralResourceName: "bar")
        }
        
        menu.addItem(withTitle: "更换背景", action:  #selector(self.setImage), keyEquivalent: "")
        
        
        childmenu.addItem(withTitle: "每天", action: #selector(self.setUpdateBgTime(nsmenuitem:)), keyEquivalent: "")
        childmenu.addItem(withTitle: "12小时", action: #selector(self.setUpdateBgTime(nsmenuitem:)), keyEquivalent: "")
        childmenu.addItem(withTitle: "6小时", action: #selector(self.setUpdateBgTime(nsmenuitem:)), keyEquivalent: "")
        childmenu.addItem(withTitle: "3小时", action: #selector(self.setUpdateBgTime(nsmenuitem:)), keyEquivalent: "")
        childmenu.addItem(withTitle: "关闭", action: #selector(self.setUpdateBgTime(nsmenuitem:)), keyEquivalent: "")
        
        let miten = NSMenuItem(title: "自动切换", action: nil, keyEquivalent: "")
        miten.submenu = childmenu
        
        childmenu.items[0].tag = 0
        childmenu.items[1].tag = 1
        childmenu.items[2].tag = 2
        childmenu.items[3].tag = 3
        childmenu.items[4].tag = 4
        
        menu.addItem(miten)
        menu.addItem(NSMenuItem.separator())
        menu.addItem(withTitle: "退出", action: #selector(quit), keyEquivalent: "")
        status.menu = menu
        
        timerImage()
        
    }
    
    func timerImage(){
        let tag = UserDefaults.standard.value(forKey: "updateTime")
        
        if tag != nil {
            let newtag = tag as! Int
            var time = 0
            if newtag != 4 {
                switch newtag {
                case 0:
                    time = 24 * 60 * 60
                    break
                case 1:
                    time = 12 * 60 * 60
                    break
                case 2:
                    time = 6 * 60 * 60
                    break
                case 3:
                    time = 3 * 60 * 60
                    break
                default:
                    break
                }
                timer = Timer.scheduledTimer(timeInterval: TimeInterval(time), target: self, selector: #selector(self.setImage), userInfo: nil, repeats: true)
            }else {
                childmenu.items[4].state = .on
                timer?.invalidate()
                timer = nil
            }
        } else {
            timer?.invalidate()
            timer = nil
            childmenu.items[4].state = .on
        }
    }
    
    @objc func setUpdateBgTime(nsmenuitem: NSMenuItem){
        let tag = nsmenuitem.tag
        for i in 0..<5 {
            childmenu.items[i].state = .off
        }
        childmenu.items[tag].state = .on
        
        UserDefaults.standard.set(tag, forKey: "updateTime")
        
        timerImage()
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }
    
    @objc func setImage(){
        DownImage.shared().getBiYingBGJson()
    }
    

    
    @objc func quit(){
        NSApplication.shared.terminate(self)
    }


}

